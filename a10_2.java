/*
@author: Dewei Chen 
@date: 3-23-2012
@class: CIS27A
@instructor: Dave Harden
@filename: a10_2.java
@description: This program will count the number of characters 
(excluding control characters '\r' and '\n'), words, and lines, 
in a user specified file. Words are separated by spaces, tabs, 
carriage return, or line-feed.
 
*/

import java.util.Scanner;

public class a10_2 {

	public static void main(String[] args) throws Exception{
		
		Scanner input1 = new Scanner(System.in);
		
		System.out.print("Enter file directory and name: ");
		java.io.File file = new java.io.File(input1.next());
		//Create a scanner for each item to be counted so it starts at the first word.
		Scanner s1 = new Scanner(file);
		Scanner s2 = new Scanner(file);
		Scanner s3 = new Scanner(file);
		
		System.out.println("File " + file.getName() + " has");
		System.out.println(countCharacters(s1) + " characters");
		System.out.println(countWords(s2) + " words");
		System.out.println(countLines(s3) + " lines");
		
	}
	
	
	
	
	
	
	
	
	
	
	//This method counts the number of characters in the file
	//including spaces except \n and \r characters.
	public static int countCharacters(Scanner s1) {
		
		int count = 0;
		String tempString;
		
		while (s1.hasNext()) {
			
			//Temporary store a new line from the text
			tempString = s1.nextLine();
			
			for(int i=1 ; i<tempString.length() ; i++) {
				
				if (tempString.charAt(i) != '\n' && tempString.charAt(i) != '\r')
					count++;
			
			}		
			
		}
		
		s1.close();
		
		return count;
		
	}
	
	
	
	
	
	
	
	
	
	
	//This method counts the number of words in the file
	public static int countWords(Scanner s2) {
		
		int count = 0;
		
		while (s2.hasNext()) {
			
			//Move to next token
			s2.next();
			count++;
		
		}
		
		return count;
		
	}
	
	
	
	
	
	
	
	
	
	
	//This method counts the number of lines in the file.
	public static int countLines(Scanner s3) {
		
		int count = 0;
		
		while (s3.hasNext()) {
			
			//Move to next line
			s3.nextLine();
			count++;
			
		}
		
		return count;
		
	}

}
