/*
@author: Dewei Chen 
@date: 3-23-2012
@class: CIS27A
@instructor: Dave Harden
@filename: a10_1.java
@description: This program prompts the user to enter a string 
followed by a character and then it will count and displays 
the number of occurrences of the character in the string.
*/

import java.util.Scanner;

public class a10_1 {
	
	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);
		String str = new String();
		char a;
		
		System.out.print("Enter a string and a letter to be counted: ");
		str = input.next();
		a = input.next().charAt(0);
		
		System.out.println("There are " + count(str,a) + " '" + a + "' in the string.");
		
	}
	
	
	
	
	
	
	
	
	
	
	
	//This method takes a string and a character and counts the number of
	//occurences of the character in the string
	public static int count(String str, char a) {
		
		int count = 0;
		int fromIndex = -1;
		
		//continue looping if indexOf() returns a non negative number:
		//meaning the character is found.
		while((fromIndex = str.indexOf(a,fromIndex+1)) >= 0) 
			count++;
			
		return count;
		
	}

}
